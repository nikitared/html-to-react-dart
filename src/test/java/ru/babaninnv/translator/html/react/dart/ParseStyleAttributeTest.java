package ru.babaninnv.translator.html.react.dart;

import org.attoparser.config.ParseConfiguration;
import org.attoparser.dom.DOMMarkupParser;
import org.attoparser.dom.Document;
import org.testng.annotations.Test;

import ru.babaninnv.translator.html.react.dart.dom.DOMDartWriter;
import ru.babaninnv.translator.html.react.dart.templates.DartClassWriter;

import java.io.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by NikitaRed on 20.03.2016.
 */
@Test
public class ParseStyleAttributeTest {

  @Test
  public void testParseStyleAttribute() {
    try {
      String htmlFileName = "src/test/resources/test_parse_style_attribute.html";

      Writer writer = new StringWriter();

      final InputStream is = new FileInputStream(new File(htmlFileName));
      final Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

      DOMMarkupParser parser = new DOMMarkupParser(ParseConfiguration.htmlConfiguration());
      Document document = parser.parse(reader);

      DOMDartWriter.write(document, writer);

      assertThat(writer.toString()).contains("div({\"style\": {\"padding\": \"10px\", \"color\": \"aqua\"}},");
      assertThat(writer.toString()).contains("div({\"style\": {\"paddingTop\": \"10px\", \"paddingBottom\": \"10px\", \"color\": \"aqua\"}}, ");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
