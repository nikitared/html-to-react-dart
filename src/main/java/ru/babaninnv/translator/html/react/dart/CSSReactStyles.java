package ru.babaninnv.translator.html.react.dart;

import org.attoparser.util.TextUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Structure gets from org.attoparser.HtmlElements
 */
public class CSSReactStyles {
  private static final HtmlReactAttributeRepository STYLES = new HtmlReactAttributeRepository();

  static final Set<CSSReactStyle> ALL_REACT_STYLES;

  static final CSSReactStyle BACKGROUND_ATTACHMENT = new CSSReactStyle("background-attachment", "backgroundAttachment");
  static final CSSReactStyle BACKGROUND_CLIP = new CSSReactStyle("background-clip", "backgroundClip");
  static final CSSReactStyle BACKGROUND_COLOR = new CSSReactStyle("background-color", "backgroundColor");
  static final CSSReactStyle BACKGROUND_IMAGE = new CSSReactStyle("background-image", "backgroundImage");
  static final CSSReactStyle BACKGROUND_ORIGIN = new CSSReactStyle("background-origin", "backgroundOrigin");
  static final CSSReactStyle BACKGROUND_POSITION = new CSSReactStyle("background-position", "backgroundPosition");
  static final CSSReactStyle BACKGROUND_POSITION_X = new CSSReactStyle("background-position-x", "backgroundPositionX");
  static final CSSReactStyle BACKGROUND_POSITION_Y = new CSSReactStyle("background-position-y", "backgroundPositionY");
  static final CSSReactStyle BACKGROUND_REPEAT = new CSSReactStyle("background-repeat", "backgroundRepeat");
  static final CSSReactStyle BACKGROUND_SIZE = new CSSReactStyle("background-size", "backgroundSize");
  static final CSSReactStyle BORDER_BOTTOM = new CSSReactStyle("border-bottom", "borderBottom");
  static final CSSReactStyle BORDER_BOTTOM_COLOR = new CSSReactStyle("border-bottom-color", "borderBottomColor");
  static final CSSReactStyle BORDER_BOTTOM_LEFT_RADIUS = new CSSReactStyle("border-bottom-left-radius", "borderBottomLeftRadius");
  static final CSSReactStyle BORDER_BOTTOM_RIGHT_RADIUS = new CSSReactStyle("border-bottom-right-radius", "borderBottomRightRadius");
  static final CSSReactStyle BORDER_BOTTOM_STYLE = new CSSReactStyle("border-bottom-style", "borderBottomStyle");
  static final CSSReactStyle BORDER_BOTTOM_WIDTH = new CSSReactStyle("border-bottom-width", "borderBottomWidth");
  static final CSSReactStyle BORDER_COLLAPSE = new CSSReactStyle("border-collapse", "borderCollapse");
  static final CSSReactStyle BORDER_COLOR = new CSSReactStyle("border-color", "borderColor");
  static final CSSReactStyle BORDER_IMAGE = new CSSReactStyle("border-image", "borderImage");
  static final CSSReactStyle BORDER_LEFT = new CSSReactStyle("border-left", "borderLeft");
  static final CSSReactStyle BORDER_LEFT_COLOR = new CSSReactStyle("border-left-color", "borderLeftColor");
  static final CSSReactStyle BORDER_LEFT_STYLE = new CSSReactStyle("border-left-style", "borderLeftStyle");
  static final CSSReactStyle BORDER_LEFT_WIDTH = new CSSReactStyle("border-left-width", "borderLeftWidth");
  static final CSSReactStyle BORDER_RADIUS = new CSSReactStyle("border-radius", "borderRadius");
  static final CSSReactStyle BORDER_RIGHT = new CSSReactStyle("border-right", "borderRight");
  static final CSSReactStyle BORDER_RIGHT_COLOR = new CSSReactStyle("border-right-color", "borderRightColor");
  static final CSSReactStyle BORDER_RIGHT_STYLE = new CSSReactStyle("border-right-style", "borderRightStyle");
  static final CSSReactStyle BORDER_RIGHT_WIDTH = new CSSReactStyle("border-right-width", "borderRightWidth");
  static final CSSReactStyle BORDER_SPACING = new CSSReactStyle("border-spacing", "borderSpacing");
  static final CSSReactStyle BORDER_STYLE = new CSSReactStyle("border-style", "borderStyle");
  static final CSSReactStyle BORDER_TOP = new CSSReactStyle("border-top", "borderTop");
  static final CSSReactStyle BORDER_TOP_COLOR = new CSSReactStyle("borderTopColor", "borderTopColor");
  static final CSSReactStyle BORDER_TOP_LEFT_RADIUS = new CSSReactStyle("borderTopLeftRadius", "borderTopLeftRadius");
  static final CSSReactStyle BORDER_TOP_RIGHT_RADIUS = new CSSReactStyle("borderTopRightRadius", "borderTopRightRadius");
  static final CSSReactStyle BORDER_TOP_STYLE = new CSSReactStyle("border-top-style", "borderTopStyle");
  static final CSSReactStyle BORDER_TOP_WIDTH = new CSSReactStyle("border-top-width", "borderTopWidth");
  static final CSSReactStyle BORDER_WIDTH = new CSSReactStyle("border-width", "borderWidth");
  static final CSSReactStyle BOX_SHADOW = new CSSReactStyle("box-shadow", "boxShadow");
  static final CSSReactStyle BOX_SIZING = new CSSReactStyle("box-sizing", "boxSizing");
  static final CSSReactStyle CAPTION_SIDE = new CSSReactStyle("caption-side", "captionSide");
  static final CSSReactStyle COLUMN_COUNT = new CSSReactStyle("column-count", "columnCount");
  static final CSSReactStyle COLUMN_GAP = new CSSReactStyle("column-gap", "columnGap");
  static final CSSReactStyle COLUMN_RULE = new CSSReactStyle("column-rule", "columnRule");
  static final CSSReactStyle COLUMN_WIDTH = new CSSReactStyle("column-width", "columnWidth");
  static final CSSReactStyle COUNTER_INCREMENT = new CSSReactStyle("counter-increment", "counterIncrement");
  static final CSSReactStyle COUNTER_RESET = new CSSReactStyle("counter-reset", "counterReset");
  static final CSSReactStyle EMPTY_CELLS = new CSSReactStyle("empty-cells", "emptyCells");
  static final CSSReactStyle FONT_FAMILY = new CSSReactStyle("font-family", "fontFamily");
  static final CSSReactStyle FONT_SIZE = new CSSReactStyle("font-size", "fontSize");
  static final CSSReactStyle FONT_STRETCH = new CSSReactStyle("font-stretch", "fontStretch");
  static final CSSReactStyle FONT_STYLE = new CSSReactStyle("font-style", "fontStyle");
  static final CSSReactStyle FONT_VARIANT = new CSSReactStyle("font-variant", "fontVariant");
  static final CSSReactStyle FONT_WEIGHT = new CSSReactStyle("font-weight", "fontWeight");
  static final CSSReactStyle HASLAYOUT = new CSSReactStyle("hasLayout", "hasLayout");
  static final CSSReactStyle IMAGE_RENDERING = new CSSReactStyle("image-rendering", "imageRendering");
  static final CSSReactStyle LETTER_SPACING = new CSSReactStyle("letter-spacing", "letterSpacing");
  static final CSSReactStyle LINE_HEIGHT = new CSSReactStyle("line-height", "lineHeight");
  static final CSSReactStyle LIST_STYLE = new CSSReactStyle("list-style", "listStyle");
  static final CSSReactStyle LIST_STYLE_IMAGE = new CSSReactStyle("list-style-image", "listStyleImage");
  static final CSSReactStyle LIST_STYLE_POSITION = new CSSReactStyle("list-style-position", "listStylePosition");
  static final CSSReactStyle LIST_STYLE_TYPE = new CSSReactStyle("list-style-type", "listStyleType");
  static final CSSReactStyle MARGIN_BOTTOM = new CSSReactStyle("margin-bottom", "marginBottom");
  static final CSSReactStyle MARGIN_LEFT = new CSSReactStyle("margin-left", "marginLeft");
  static final CSSReactStyle MARGIN_RIGHT = new CSSReactStyle("margin-right", "marginRight");
  static final CSSReactStyle MARGIN_TOP = new CSSReactStyle("margin-top", "marginTop");
  static final CSSReactStyle MAX_HEIGHT = new CSSReactStyle("max-height", "maxHeight");
  static final CSSReactStyle MAX_WIDTH = new CSSReactStyle("max-width", "maxWidth");
  static final CSSReactStyle MIN_HEIGHT = new CSSReactStyle("min-height", "minHeight");
  static final CSSReactStyle MIN_WIDTH = new CSSReactStyle("min-width", "minWidth");
  static final CSSReactStyle OUTLINE_COLOR = new CSSReactStyle("outline-color", "outlineColor");
  static final CSSReactStyle OUTLINE_OFFSET = new CSSReactStyle("outline-offset", "outlineOffset");
  static final CSSReactStyle OUTLINE_STYLE = new CSSReactStyle("outline-style", "outlineStyle");
  static final CSSReactStyle OUTLINE_WIDTH = new CSSReactStyle("outline-width", "outlineWidth");
  static final CSSReactStyle OVERFLOW_X = new CSSReactStyle("overflow-x", "overflowX");
  static final CSSReactStyle OVERFLOW_Y = new CSSReactStyle("overflow-y", "overflowY");
  static final CSSReactStyle PADDING_BOTTOM = new CSSReactStyle("padding-bottom", "paddingBottom");
  static final CSSReactStyle PADDING_LEFT = new CSSReactStyle("padding-left", "paddingLeft");
  static final CSSReactStyle PADDING_RIGHT = new CSSReactStyle("padding-right", "paddingRight");
  static final CSSReactStyle PADDING_TOP = new CSSReactStyle("padding-top", "paddingTop");
  static final CSSReactStyle PAGE_BREAK_AFTER = new CSSReactStyle("page-break-after", "pageBreakAfter");
  static final CSSReactStyle PAGE_BREAK_BEFORE = new CSSReactStyle("page-break-before", "pageBreakBefore");
  static final CSSReactStyle PAGE_BREAK_INSIDE = new CSSReactStyle("page-break-inside", "pageBreakInside");
  static final CSSReactStyle DLIGHT_COLOR = new CSSReactStyle("dlight-color", "dlightColor");
  static final CSSReactStyle SCROLLBAR_ARRO_COLOR = new CSSReactStyle("scrollbar-arrow-color", "scrollbarArrowColor");
  static final CSSReactStyle SCROLLBAR_BASE_COLOR = new CSSReactStyle("scrollbar-base-color", "scrollbarBaseColor");
  static final CSSReactStyle SCROLLBAR_DARKSHADOW_COLOR = new CSSReactStyle("scrollbar-darkshadow-color", "scrollbarDarkshadowColor");
  static final CSSReactStyle SCROLLBAR_FACE_COLOR = new CSSReactStyle("scrollbar-face-color", "scrollbarFaceColor");
  static final CSSReactStyle SCROLLBAR_HIGHLIGHT_COLOR = new CSSReactStyle("scrollbar-highlight-color", "scrollbarHighlightColor");
  static final CSSReactStyle SCROLLBAR_SHADOW_COLOR = new CSSReactStyle("scrollbar-shadow-color", "scrollbarShadowColor");
  static final CSSReactStyle SCROLLBAR_TRACK_COLOR = new CSSReactStyle("scrollbar-track-color", "scrollbarTrackColor");
  static final CSSReactStyle TAB_SIZE = new CSSReactStyle("tab-size", "tabSize");
  static final CSSReactStyle TABLE_LAYOUT = new CSSReactStyle("table-layout", "tableLayout");
  static final CSSReactStyle TEXT_ALIGN = new CSSReactStyle("text-align", "textAlign");
  static final CSSReactStyle TEXT_ALIGN_LAST = new CSSReactStyle("text-align-last", "textAlignLast");
  static final CSSReactStyle TEXT_DECORATION = new CSSReactStyle("text-decoration", "textDecoration");
  static final CSSReactStyle TEXT_DECORATION_COLOR = new CSSReactStyle("text-decoration-color", "textDecorationColor");
  static final CSSReactStyle TEXT_DECORATION_LINE = new CSSReactStyle("text-decoration-line", "textDecorationLine");
  static final CSSReactStyle TEXT_DECORATION_STYLE = new CSSReactStyle("text-decoration-style", "textDecorationStyle");
  static final CSSReactStyle TEXT_INDENT = new CSSReactStyle("text-indent", "textIndent");
  static final CSSReactStyle TEXT_OVERFLOW = new CSSReactStyle("text-overflow", "textOverflow");
  static final CSSReactStyle TEXT_SHADOW = new CSSReactStyle("text-shadow", "textShadow");
  static final CSSReactStyle TEXT_TRANSFORM = new CSSReactStyle("text-transform", "textTransform");
  static final CSSReactStyle TRANSFORM_ORIGIN = new CSSReactStyle("transform-origin", "transformOrigin");
  static final CSSReactStyle TRANSFORM_STYLE = new CSSReactStyle("transform-style", "transformStyle");
  static final CSSReactStyle TRANSITION_DELAY = new CSSReactStyle("transition-delay", "transitionDelay");
  static final CSSReactStyle TRANSITION_PROPERTY = new CSSReactStyle("transition-property", "transitionProperty");
  static final CSSReactStyle TRANSITION_TIMING_FUNCTION = new CSSReactStyle("transition-timing-function", "transitionTimingFunction");
  static final CSSReactStyle UNICODE_BIDI = new CSSReactStyle("unicode-bidi", "unicodeBidi");
  static final CSSReactStyle VERTICAL_ALIGN = new CSSReactStyle("vertical-align", "verticalAlign");
  static final CSSReactStyle WHITE_SPACE = new CSSReactStyle("white-space", "whiteSpace");
  static final CSSReactStyle WORD_BREAK = new CSSReactStyle("word-break", "wordBreak");
  static final CSSReactStyle WORD_SPACING = new CSSReactStyle("word-spacing", "wordSpacing");
  static final CSSReactStyle WORD_WRAP = new CSSReactStyle("word-wrap", "wordWrap");
  static final CSSReactStyle WRITING_MODE = new CSSReactStyle("writing-mode", "writingMode");
  static final CSSReactStyle Z_INDEX = new CSSReactStyle("z-index", "zIndex");

  static {

    ALL_REACT_STYLES =
            Collections.unmodifiableSet(new LinkedHashSet<CSSReactStyle>(Arrays.asList(new CSSReactStyle[]{
                    BACKGROUND_ATTACHMENT, BACKGROUND_CLIP, BACKGROUND_COLOR, BACKGROUND_IMAGE, BACKGROUND_ORIGIN, BACKGROUND_POSITION, BACKGROUND_POSITION_X,
                    BACKGROUND_POSITION_Y, BACKGROUND_REPEAT, BACKGROUND_SIZE, BORDER_BOTTOM, BORDER_BOTTOM_COLOR, BORDER_BOTTOM_LEFT_RADIUS, BORDER_BOTTOM_RIGHT_RADIUS, BORDER_BOTTOM_STYLE, BORDER_BOTTOM_WIDTH, BORDER_COLLAPSE, BORDER_COLOR, BORDER_IMAGE, BORDER_LEFT, BORDER_LEFT_COLOR,
                    BORDER_LEFT_STYLE, BORDER_LEFT_WIDTH, BORDER_RADIUS, BORDER_RIGHT, BORDER_RIGHT_COLOR, BORDER_RIGHT_STYLE, BORDER_RIGHT_WIDTH, BORDER_SPACING, BORDER_STYLE, BORDER_TOP, BORDER_TOP_COLOR, BORDER_TOP_LEFT_RADIUS, BORDER_TOP_RIGHT_RADIUS, BORDER_TOP_STYLE,
                    BORDER_TOP_WIDTH, BORDER_WIDTH, BOX_SHADOW, BOX_SIZING, CAPTION_SIDE, COLUMN_COUNT, COLUMN_GAP, COLUMN_RULE, COLUMN_WIDTH, COUNTER_INCREMENT, COUNTER_RESET, EMPTY_CELLS, FONT_FAMILY, FONT_SIZE, FONT_STRETCH, FONT_STYLE, FONT_VARIANT, FONT_WEIGHT, HASLAYOUT, IMAGE_RENDERING, LETTER_SPACING,
                    LINE_HEIGHT, LIST_STYLE, LIST_STYLE_IMAGE, LIST_STYLE_POSITION, LIST_STYLE_TYPE, MARGIN_BOTTOM, MARGIN_LEFT, MARGIN_RIGHT, MARGIN_TOP, MAX_HEIGHT, MAX_WIDTH, MIN_HEIGHT, MIN_WIDTH, OUTLINE_COLOR, OUTLINE_OFFSET, OUTLINE_STYLE, OUTLINE_WIDTH,
                    OVERFLOW_X, OVERFLOW_Y, PADDING_BOTTOM, PADDING_LEFT, PADDING_RIGHT, PADDING_TOP, PAGE_BREAK_AFTER, PAGE_BREAK_BEFORE, PAGE_BREAK_INSIDE, DLIGHT_COLOR, SCROLLBAR_ARRO_COLOR, SCROLLBAR_BASE_COLOR, SCROLLBAR_DARKSHADOW_COLOR, SCROLLBAR_FACE_COLOR, SCROLLBAR_HIGHLIGHT_COLOR, SCROLLBAR_SHADOW_COLOR, SCROLLBAR_TRACK_COLOR, TAB_SIZE, TABLE_LAYOUT, TEXT_ALIGN,
                    TEXT_ALIGN_LAST, TEXT_DECORATION, TEXT_DECORATION_COLOR, TEXT_DECORATION_LINE, TEXT_DECORATION_STYLE, TEXT_INDENT, TEXT_OVERFLOW, TEXT_SHADOW, TEXT_TRANSFORM, TRANSFORM_ORIGIN, TRANSFORM_STYLE, TRANSITION_DELAY,
                    TRANSITION_PROPERTY, TRANSITION_TIMING_FUNCTION, UNICODE_BIDI, VERTICAL_ALIGN, WHITE_SPACE, WORD_BREAK, WORD_SPACING, WORD_WRAP, WRITING_MODE, Z_INDEX})));

    for (final CSSReactStyle element : ALL_REACT_STYLES) {
      STYLES.storeStandardElement(element);
    }
  }

  /*
   * Note this will always be case-insensitive, because we are dealing with HTML.
   */
  public static CSSReactStyle forName(final char[] elementNameBuffer, final int offset, final int len) {
    if (elementNameBuffer == null) {
      throw new IllegalArgumentException("Buffer cannot be null");
    }
    return STYLES.getElement(elementNameBuffer, offset, len);
  }

  private CSSReactStyles() {
    super();
  }


  /*
   * This repository class is thread-safe. The reason for this is that it not only contains the
   * standard elements, but will also contain new instances of HtmlElement created during parsing
   * (created when asking the repository for them when they do not exist yet. As any thread can
   * create a new element, this has to be lock-protected.
   */
  static final class HtmlReactAttributeRepository {

    private final List<CSSReactStyle> standardRepository; // read-only, no sync needed
    private final List<CSSReactStyle> repository; // read-write, sync will be needed

    private final ReadWriteLock lock = new ReentrantReadWriteLock(true);
    private final Lock readLock = this.lock.readLock();
    private final Lock writeLock = this.lock.writeLock();

    HtmlReactAttributeRepository() {
      this.standardRepository = new ArrayList<CSSReactStyle>();
      this.repository = new ArrayList<CSSReactStyle>();
    }


    CSSReactStyle getElement(final char[] text, final int offset, final int len) {

      /*
       * We first try to find it in the repository containing the standard elements, which does not
       * need any synchronization.
       */
      int index = binarySearch(this.standardRepository, text, offset, len);

      if (index >= 0) {
        return this.standardRepository.get(index);
      }

      this.readLock.lock();
      try {
        index = binarySearch(this.repository, text, offset, len);
        if (index >= 0) {
          return this.repository.get(index);
        }
      } finally {
        this.readLock.unlock();
      }

      /*
       * NOT FOUND. We need to obtain a write lock and store the text
       */
      this.writeLock.lock();
      try {
        return storeElement(text, offset, len);
      } finally {
        this.writeLock.unlock();
      }
    }


    private CSSReactStyle storeElement(final char[] text, final int offset, final int len) {

      final int index = binarySearch(this.repository, text, offset, len);
      if (index >= 0) {
        // It was already added while we were waiting for the lock!
        return this.repository.get(index);
      }

      final CSSReactStyle element =
              new CSSReactStyle(new String(text, offset, len).toLowerCase(), new String(text, offset, len).toLowerCase());

      // binary Search returned (-(insertion point) - 1)
      this.repository.add(((index + 1) * -1), element);

      return element;

    }


    private CSSReactStyle storeStandardElement(final CSSReactStyle element) {

      // This method will only be called from within the HtmlElements class itself, during
      // initialization of
      // standard elements.

      this.standardRepository.add(element);
      this.repository.add(element);
      Collections.sort(this.standardRepository, ReactAttributeComparator.INSTANCE);
      Collections.sort(this.repository, ReactAttributeComparator.INSTANCE);

      return element;

    }


    private static int binarySearch(final List<CSSReactStyle> values, final char[] text, final int offset, final int len) {

      int low = 0;
      int high = values.size() - 1;

      int mid, cmp;
      char[] midVal;

      while (low <= high) {

        mid = (low + high) >>> 1;
        midVal = values.get(mid).cssName;

        cmp = TextUtil.compareTo(false, midVal, 0, midVal.length, text, offset, len);

        if (cmp < 0) {
          low = mid + 1;
        } else if (cmp > 0) {
          high = mid - 1;
        } else {
          // Found!!
          return mid;
        }

      }

      return -(low + 1); // Not Found!! We return (-(insertion point) - 1), to guarantee all
      // non-founds are < 0

    }


    private static class ReactAttributeComparator implements Comparator<CSSReactStyle> {

      private static ReactAttributeComparator INSTANCE = new ReactAttributeComparator();

      public int compare(final CSSReactStyle o1, final CSSReactStyle o2) {
        return TextUtil.compareTo(false, o1.cssName, o2.cssName);
      }
    }

  }


}
