package ru.babaninnv.translator.html.react.dart;

public class CSSReactStyle {
  final char[] cssName;
  final char[] reactCssName;

  public CSSReactStyle(final String cssName, final String reactCssName) {
    super();
    if (cssName == null) { throw new IllegalArgumentException("CSS name cannot be null"); }
    if (reactCssName == null) { throw new IllegalArgumentException("React CSS name cannot be null"); }
    this.cssName = cssName.toLowerCase().toCharArray();
    this.reactCssName = reactCssName.toCharArray();
  }

  public char[] getReactName() {
    return reactCssName;
  }

  @Override
  public String toString() {
      final StringBuilder strBuilder = new StringBuilder();
      strBuilder.append(this.cssName);
      strBuilder.append(' ');
      strBuilder.append('-');
      strBuilder.append('>');
      strBuilder.append(' ');
      strBuilder.append(this.reactCssName);
      return strBuilder.toString();
  }
}
